#!/bin/bash
# $1: QUE_ID = Folder Name

# set the name of dockerImage
dockerName="appr/parser:v1"
containerName=appr_orgnt_agent

########################################
# create backup folder if not exists   #
########################################

bash /Appier/run_download_orgnt.sh> info.log 2>&1 apikey #agent originate

###################################
# clean old files                 #
###################################
# keep files for 180 days

# delete files before 180 days
#find $path -type d -name "QUE-*" -mtime +$clean_before_days -exec rm -r '{}' \;

###################################
# check Docker is running         #
###################################
if docker stats --no-stream | grep $containerName
then
    echo "docker is still running"
else 
    #####################################
    # delete previous docker containers #
    #####################################
    echo "delete previous docker containers"
    docker ps -a | grep "$containerName" | awk '{print $1}' | xargs docker container rm
    echo "start docker"
    #docker run --name $containerName $dockerName python --tid $1
    docker run --name $containerName --privileged=true -d -v /Appier:/Appier  $dockerName python originate.py --orgnt agent
fi


###################################
# kill docker wait process        #
###################################
echo "kill previous docker wait process"
pids=$(ps -ef | grep "docker wait $containerName" | grep -v grep | awk '{print $2}')

if [ $pids ];
then 
    kill -9 $pids
fi

###################################
# wait for docker                 #
###################################
echo "call docker wait"
docker wait $containerName

###################################
# export docker log to a file     #
###################################
logfile=/Appier/output.log
> $logfile
docker logs $containerName &> $logfile
echo "docker finished"

###################################
# show logs from log file          #
###################################
tail -n 5000 $logfile

###################################
# check error from log file       #
###################################
rtnCode=0
if [[ $(cat $logfile | grep "Traceback" | wc -l) -ne 0 ]]
then 
    echo "Process Error: found $errCount error(s) by grep \"Traceback\""
    rtnCode=-1
fi

echo "script finished"
# no error happened, exit 0 as normal
exit $rtnCode
