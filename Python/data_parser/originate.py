from enum import Enum, unique
from datetime import date, datetime
from config import *
from argparse import ArgumentParser
import paramiko
import json
import numpy as np
import pandas as pd
import os
import time
import platform
import gzip
import warnings
warnings.filterwarnings("ignore")



@unique
class IDInfo(Enum):

    emailsha256 = 0 
    customuid = 1
    idfa = 2
    dmp_id = 3
    appier_cookie_id = 4
    Geo_Location = 5
    data_dt = 6

def id_info():
    id_info_collection = []

    for i in data:

        id_info = [None] * len(IDInfo)
        

        id_info[IDInfo.emailsha256.value] = i['emailsha256'] if len(i['emailsha256']) > 0 else None
        id_info[IDInfo.customuid.value] = i['customuid'] if len(i['customuid']) > 0 else None
        id_info[IDInfo.idfa.value] = i['idfa'] if len(i['idfa']) > 0 else None
        id_info[IDInfo.dmp_id.value] = i['dmp_id'] if len(i['dmp_id']) > 0 else None
        id_info[IDInfo.appier_cookie_id.value] = i['appier_cookie_id'] if len(i['appier_cookie_id']) > 0 else None
        id_info[IDInfo.data_dt.value] = date.today()
        id_info[IDInfo.Geo_Location.value] = '_'.join(i['Geo Location']) if len(i['Geo Location'])>0 else None
        id_info_collection.append(id_info)


    df_id_info = pd.DataFrame(id_info_collection, columns=[k.name for k in IDInfo])
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()  
    with sftp_client.open(os.path.join(OUTPUT,'df_id_info_{0}_{1}.csv'.format(args.orgnt,time.strftime("%Y%m%d"))), "w") as f:
        f.write(df_id_info.to_csv(index=False, encoding='utf-8',sep='\x06')) 
    return df_id_info



if __name__ == '__main__':

    record_time = [datetime.now()]
    msg = ('Main Start Originate Parser Procudure in {}'.format(
        platform.system() + ' ' + platform.release()))
    parser = ArgumentParser()
    parser.add_argument("--orgnt", help="appier originate agent",required=True)
    args = parser.parse_args()
    orgnt = args.orgnt    
    
    
    with gzip.open('download/Appier_orgnt_{0}_{1}.json.gz'.format(args.orgnt,time.strftime("%Y%m%d")), 'rb') as f: #originate shell will send orgnt argument here
        data = f.readlines()
        data = list(map(json.loads, data))
    
    id_info()
    record_time.append(datetime.now())
    msg = 'Main Finish Originate Parser Procedure Spend {} Minutes'.format(
        (record_time[-1] - record_time[0]).seconds / 60)
    print(msg)