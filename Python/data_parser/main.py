from enum import Enum, unique
from datetime import date, datetime
from config import *
import paramiko
import json
import numpy as np
import pandas as pd
import os
import time
import platform
import gzip
import warnings
warnings.filterwarnings("ignore")



@unique
class IDInfo(Enum):

    emailsha256 = 0 
    customuid = 1
    idfa = 2
    dmp_id = 3
    appier_cookie_id = 4
    Geo_Location = 5
    data_dt = 6


@unique
class KeywordsCol(Enum):

    Out_of_network_keywords = 2
    In_network_keywords = 3
    Custom_keywords = 4
    Keyword_timeframe = 5
    data_dt = 6

@unique
class OutOfNetWorkRFM(Enum):

    keyword = 2
    frequency = 3 #Out_of_network_keywords_frequency
    recency = 4 #Out_of_network_keywords_recency
    Recency_Date = 5 #Out_of_network_Keyword_Recency_Date
    data_dt = 6
    
@unique
class InNetworkRFM(Enum):
    
    keyword = 3
    In_network_keywords_frequency = 4
    In_network_keywords_recency = 5
    In_network_Keyword_Recency_Date = 6
    data_dt = 7
@unique
class CustomKeyword(Enum):
    
    keyword = 1
    Keyword_Frequency = 2
    Keyword_Recency = 3
    Keyword_Recency_Date = 4
    data_dt = 5

@unique    
class InterestRFM(Enum):
    
    category = 2
    interest = 3
    keyword = 4
    frequency = 5
    recency = 6
    recency_date = 7
    data_dt = 8

def id_info():
    id_info_collection = []

    for i in data:

        id_info = [None] * len(IDInfo)
        

        id_info[IDInfo.emailsha256.value] = i['emailsha256'] if len(i['emailsha256']) > 0 else None
        id_info[IDInfo.customuid.value] = i['customuid'] if len(i['customuid']) > 0 else None
        id_info[IDInfo.idfa.value] = i['idfa'] if len(i['idfa']) > 0 else None
        id_info[IDInfo.dmp_id.value] = i['dmp_id'] if len(i['dmp_id']) > 0 else None
        id_info[IDInfo.appier_cookie_id.value] = i['appier_cookie_id'] if len(i['appier_cookie_id']) > 0 else None
        id_info[IDInfo.data_dt.value] = date.today()
        id_info[IDInfo.Geo_Location.value] = '_'.join(i['Geo Location']) if len(i['Geo Location'])>0 else None
        id_info_collection.append(id_info)


    df_id_info = pd.DataFrame(id_info_collection, columns=[k.name for k in IDInfo])
    df_id_info.to_csv(index=False, encoding='utf-8',sep='\x06')
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()  
    with sftp_client.open(os.path.join(OUTPUT,'df_id_info_{}.csv'.format(time.strftime("%Y%m%d"))), "w") as f:
        f.write(df_id_info.to_csv(index=False, encoding='utf-8',sep='\x06')) 
    return df_id_info


def keywords_info():

    keywords_collection = []

    for i in data:

        keywords_info = [None] * (len(KeywordsCol) + 2) 
        
        keywords_info[IDInfo.emailsha256.value] = i['emailsha256'] if len(i['emailsha256'])>0 else None
        keywords_info[IDInfo.customuid.value] = i['customuid'] if len(i['customuid'])>0 else None
        keywords_info[KeywordsCol.Out_of_network_keywords.value] = '_'.join(i['Out of network keywords']) if len(i['Out of network keywords'])>0 else None
        keywords_info[KeywordsCol.In_network_keywords.value] = '_'.join(i['In network keywords']) if len(i['In network keywords'])>0 else None
        keywords_info[KeywordsCol.Custom_keywords.value] = '_'.join(i['Custom keywords']) if len(i['Custom keywords'])>0 else None
        keywords_info[KeywordsCol.Keyword_timeframe.value] = i['Keyword timeframe'].split(' ')[0] if len(i['Keyword timeframe']) > 0 else None
        keywords_info[KeywordsCol.data_dt.value] = date.today()
        keywords_collection.append(keywords_info)
        

    keywords_column_name = [IDInfo.emailsha256.name]+[IDInfo.customuid.name]+[k.name for k in KeywordsCol]
    df_keywords_info = pd.DataFrame(keywords_collection, columns=keywords_column_name)
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()  
    with sftp_client.open(os.path.join(OUTPUT,'df_keywords_info_{}.csv'.format(time.strftime("%Y%m%d"))), "w") as f:
        f.write(df_keywords_info.to_csv(index=False, encoding='utf-8',sep='\x06')) 
    return df_keywords_info


def rfm_info():

    out_of_network_rfm_collection = []

    for i in data:
        email_sha256 = i['emailsha256'] if len(i['emailsha256'])>0 else None

        
        for k in i['Out of network keywords']:
            oon_rfm = [None] * (len(OutOfNetWorkRFM)+2) 
            
            oon_rfm[IDInfo.emailsha256.value] = email_sha256
            oon_rfm[IDInfo.customuid.value] = i['customuid'] if len( i['customuid'])>0 else None
            oon_rfm[OutOfNetWorkRFM.keyword.value] = k
            oon_rfm[OutOfNetWorkRFM.frequency.value] = i['Out of network keywords frequency'][k]
            oon_rfm[OutOfNetWorkRFM.recency.value] = i['Out of network keywords recency'][k]
            oon_rfm[OutOfNetWorkRFM.Recency_Date.value] = i['Out of network Keyword Recency Date'][k]
            oon_rfm[OutOfNetWorkRFM.data_dt.value] = date.today()
            
            out_of_network_rfm_collection.append(oon_rfm)
            

    oon_rfm_col_name = [IDInfo.emailsha256.name]+[IDInfo.customuid.name]+[k.name for k in OutOfNetWorkRFM]
    df_oon_rfm_info = pd.DataFrame(out_of_network_rfm_collection, columns=oon_rfm_col_name)
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()  
    with sftp_client.open(os.path.join(OUTPUT,'df_oon_rfm_info_{}.csv'.format(time.strftime("%Y%m%d"))), "w") as f:
        f.write(df_oon_rfm_info.to_csv(index=False, encoding='utf-8',sep='\x06'))      
    return df_oon_rfm_info

def read_interest(csv_path):
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()
    interest = pd.read_csv(sftp_client.open(csv_path) ,delimiter="|", encoding='utf8')
    return interest


def interest_info():
    interest_rfm_collection = []
    for i in data:
        email_sha256 = i['emailsha256'] if len(i['emailsha256'])>0 else None
        
        for k in i['Preset out of network Interest']:
            interest_rfm = [None] * (len(InterestRFM)+2) 
            
            interest_rfm[IDInfo.emailsha256.value] =  email_sha256
            interest_rfm[IDInfo.customuid.value] = i['customuid'] if len(i['customuid'])>0 else None
            interest_rfm[InterestRFM.category.value] = ''.join(interest[interest['Interest']==pd.Series(k).any()]['Category'])
            interest_rfm[InterestRFM.interest.value] = k['interest']
            interest_rfm[InterestRFM.keyword.value] = '_'.join(k['keywords'])
            interest_rfm[InterestRFM.frequency.value] = i['Preset out of network Interest Frequency'][k['interest']]
            interest_rfm[InterestRFM.recency.value] = i['Preset out of network Interest Recency'][k['interest']]
            interest_rfm[InterestRFM.recency_date.value] = i['Preset out of network Interest Recency Date'][k['interest']]
            interest_rfm[InterestRFM.data_dt.value] = date.today()
            
            interest_rfm_collection.append(interest_rfm)
            

    fasion_rfm_col_name = [IDInfo.emailsha256.name]+[IDInfo.customuid.name]+[k.name for k in InterestRFM]
    df_interest_rfm_info = pd.DataFrame(interest_rfm_collection, columns=fasion_rfm_col_name)
    UniqueNames =  df_interest_rfm_info.category.unique()
    DataFrameDict = {elem : pd.DataFrame for elem in UniqueNames}
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()    
    for cat in interest['Category'].unique():
        if cat in UniqueNames:
            for key in DataFrameDict.keys():
                DataFrameDict[key] = df_interest_rfm_info[:][df_interest_rfm_info.category == key]
                with sftp_client.open(os.path.join(OUTPUT,'keyword_'+'{0}_{1}.csv'.format(key,time.strftime("%Y%m%d"))), "w") as f:
                    f.write(DataFrameDict[key].to_csv(index=False, encoding='utf-8',sep='\x06'))             
        else:
            df = pd.DataFrame(columns = ['emailsha256','customuid','category','interest','keyword','frequency','recency','recency_date'])
            with sftp_client.open(os.path.join(OUTPUT,'keyword_other_{}.csv'.format(time.strftime("%Y%m%d"))), "w") as f:
                f.write(df.to_csv(index=False, encoding='utf-8',sep='\x06'))                
    return df_interest_rfm_info   

if __name__ == '__main__':

    record_time = [datetime.now()]
    msg = ('Main Start Parser Procudure in {}'.format(
        platform.system() + ' ' + platform.release()))
        
    with gzip.open('download/Appier_keyword_{}.json.gz'.format(time.strftime("%Y%m%d")), 'rb') as f:
        data = f.readlines()
        data = list(map(json.loads, data))
    
    id_info()
    keywords_info()
    rfm_info()
    record_time.append(datetime.now())
    msg = 'Main Finish Parser Procedure Spend {} Minutes'.format(
        (record_time[-1] - record_time[0]).seconds / 60)
    print(msg)
    interest = read_interest(REMOTE)
    interest_info()
    
    record_time.append(datetime.now())
    msg = 'Main Finish Interest Parser Procedure Total Spend {} Minutes'.format(
        (record_time[-1] - record_time[0]).seconds / 60)
    print(msg)  