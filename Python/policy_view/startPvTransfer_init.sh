#!/bin/bash
# $1: QUE_ID = Folder Name

# set the name of dockerImage
dockerName="pv/transfer:v1"
containerName=pv_$1

docker run --name $containerName --privileged=true -d -v /policyview:/policyview  $dockerName python main.py --poyear $2
