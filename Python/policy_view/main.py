from datetime import date,datetime
from collections import defaultdict
from os import path
from pandas import json_normalize
from config import *
from xml.dom import minidom
import paramiko
import os
import time
import pandas as pd
import numpy as np
import gzip
import re
import csv
import warnings
import requests
import pandas as pd
import json
import xml
import xml.etree.ElementTree as ET


ParseError =  xml.etree.ElementTree.ParseError
warnings.filterwarnings("ignore")


def diff_time(record):
    return round(((record[-1] - record[-2]).seconds) / 60, 2)


def transfer_in():
    poldata = defaultdict(list)
    chunksize = 10 ** 6

    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()


    with pd.read_csv(sftp_client.open(input_file), sep='|',header = 0, keep_default_na=False,chunksize=chunksize) as reader:
        for chunk in reader:
            poldata['row_number'].extend(chunk['row_number'])
            poldata['policy_no'].extend(chunk['policy_no'])
            poldata['coverage_no'].extend(chunk['coverage_no'])
            poldata['collect_po_type'].extend(chunk['collect_po_type'])
            poldata['po_issue_date'].extend(chunk['po_issue_date'])
            poldata['co_issue_date'].extend(chunk['co_issue_date'])
            poldata['matured_date'].extend(chunk['matured_date'])
            poldata['expired_date'].extend(chunk['expired_date'])
            poldata['po_year'].extend(chunk['po_year'])
            poldata['co_status_code'].extend(chunk['co_status_code'])
            poldata['rate_sex'].extend(chunk['rate_sex'])
            poldata['rate_age'].extend(chunk['rate_age'])
            poldata['plan_code'].extend(chunk['plan_code'])
            poldata['version'].extend(chunk['version'])
            poldata['face_amt'].extend(chunk['face_amt'])
            poldata['amtunit'].extend(chunk['amtunit'])
            poldata['face_amt_unit'].extend(chunk['face_amt_unit'])
            poldata['modx'].extend(chunk['modx'])
            poldata['mode_prem'].extend(chunk['mode_prem'])
            poldata['occupation_class'].extend(chunk['occupation_class'])
            poldata['client_id_o'].extend(chunk['client_id_o'])
            poldata['client_o_name'].extend(chunk['client_o_name'])
            poldata['client_o_birth'].extend(chunk['client_o_birth'])
            poldata['client_o_sex'].extend(chunk['client_o_sex'])
            poldata['client_id_i'].extend(chunk['client_id_i'])
            poldata['client_i_name'].extend(chunk['client_i_name'])
            poldata['client_i_birth'].extend(chunk['client_i_birth'])
            poldata['client_i_sex'].extend(chunk['client_i_sex'])
            poldata['rela_seq'].extend(chunk['rela_seq'])
            poldata['pdtcode'].extend(chunk['pdtcode'])
            poldata['kind'].extend(chunk['kind'])
            poldata['currency_1'].extend(chunk['currency_1'])
            poldata['currency_desc'].extend(chunk['currency_desc'])
            poldata['exg_rate'].extend(chunk['exg_rate'])
            poldata['pdtyear'].extend(chunk['pdtyear'])
            poldata['plstatuscode'].extend(chunk['plstatuscode'])
            poldata['company'].extend(chunk['company'])
            poldata['a1'].extend(chunk['a1'])  
            poldata['a2'].extend(chunk['a2'])  
            poldata['a3'].extend(chunk['a3'])  
            poldata['a4'].extend(chunk['a4'])  
            poldata['a5'].extend(chunk['a5'])  
            poldata['a6'].extend(chunk['a6'])  
            poldata['a7'].extend(chunk['a7'])  
            poldata['a8'].extend(chunk['a8'])  
            poldata['a10'].extend(chunk['a10'])  
            poldata['a11'].extend(chunk['a11'])  
            poldata['a12'].extend(chunk['a12'])  
            poldata['a13'].extend(chunk['a13'])  
            poldata['a14'].extend(chunk['a14'])  
            poldata['a15'].extend(chunk['a15'])  
            poldata['a17'].extend(chunk['a17'])  
            poldata['a18'].extend(chunk['a18'])  
            poldata['a19'].extend(chunk['a19'])  
            poldata['a20'].extend(chunk['a20'])  
            poldata['a21'].extend(chunk['a21'])  
            poldata['a22'].extend(chunk['a22'])  
            poldata['a23'].extend(chunk['a23'])  
            poldata['a24'].extend(chunk['a24'])  
            poldata['a25'].extend(chunk['a25'])  
            poldata['a26'].extend(chunk['a26'])  
            poldata['a27'].extend(chunk['a27'])  
            poldata['a28'].extend(chunk['a28'])  
            poldata['a29'].extend(chunk['a29'])  
            poldata['a30'].extend(chunk['a30'])  
            poldata['a31'].extend(chunk['a31'])  
            poldata['b1'].extend(chunk['b1'])  
            poldata['b2'].extend(chunk['b2'])  
            poldata['b3'].extend(chunk['b3'])  
            poldata['b4'].extend(chunk['b4'])  
            poldata['b5'].extend(chunk['b5'])  
            poldata['b6'].extend(chunk['b6'])  
            poldata['b7'].extend(chunk['b7'])  
            poldata['b8'].extend(chunk['b8'])  
            poldata['b9'].extend(chunk['b9'])  
            poldata['b10'].extend(chunk['b10'])  
            poldata['b11'].extend(chunk['b11'])  
            poldata['b12'].extend(chunk['b12'])  
            poldata['b13'].extend(chunk['b13'])  
            poldata['b14'].extend(chunk['b14'])  
            poldata['c1'].extend(chunk['c1'])  
            poldata['c2'].extend(chunk['c2'])  
            poldata['c3'].extend(chunk['c3'])  
            poldata['c4'].extend(chunk['c4'])  
            poldata['c5'].extend(chunk['c5'])  
            poldata['c6'].extend(chunk['c6'])  
            poldata['c7'].extend(chunk['c7'])  
            poldata['c8'].extend(chunk['c8'])  
            poldata['c10'].extend(chunk['c10'])  
            poldata['c11'].extend(chunk['c11'])  
            poldata['c12'].extend(chunk['c12'])  
            poldata['c13'].extend(chunk['c13'])  
            poldata['c14'].extend(chunk['c14'])  
            poldata['c15'].extend(chunk['c15'])  
            poldata['d1'].extend(chunk['d1'])  
            poldata['d2'].extend(chunk['d2'])  
            poldata['d3'].extend(chunk['d3'])  
            poldata['d4'].extend(chunk['d4'])  
            poldata['d5'].extend(chunk['d5'])  
            poldata['e1'].extend(chunk['e1'])  
            poldata['e2'].extend(chunk['e2'])  
            poldata['e3'].extend(chunk['e3'])  
            poldata['e4'].extend(chunk['e4'])  
            poldata['e5'].extend(chunk['e5'])  
            poldata['e6'].extend(chunk['e6'])  
            poldata['e7'].extend(chunk['e7'])  
            poldata['e8'].extend(chunk['e8'])  
            poldata['e9'].extend(chunk['e9'])  
            poldata['e10'].extend(chunk['e10'])  
            poldata['e11'].extend(chunk['e11'])  
            poldata['e12'].extend(chunk['e12'])  
            poldata['e13'].extend(chunk['e13'])  
            poldata['e14'].extend(chunk['e14'])  
            poldata['e15'].extend(chunk['e15'])  
            poldata['e16'].extend(chunk['e16'])  
            poldata['e17'].extend(chunk['e17'])  
            poldata['e18'].extend(chunk['e18'])  
            poldata['e19'].extend(chunk['e19'])  
            poldata['e20'].extend(chunk['e20'])            
            poldata['current_year'].extend(chunk['current_year'])    
            poldata['data_dt'].extend(chunk['data_dt'])    
    policy_data = pd.DataFrame.from_dict(poldata)
    policy_data.astype(policy_data_dtypes)
    s.close()
    print('policy data loaded')
    return policy_data
    
    
def processString(txt):
    txt = re.sub(',{2,}', ',', str(txt))
    txt = re.sub('^,|,$', '', str(txt))  
    return txt
    
def buildNewXml(x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20,x21,x22):  
    root = ET.Element("DATA")


    #root
    setting = ET.SubElement(root, "setting")
    custdata = ET.SubElement(root, "custdata")

    
    
    #setting
    setting_viewmode = ET.SubElement(setting, "viewmode")
    setting_viewmode.text = "3"
    setting_startyear = ET.SubElement(setting, "startyear")
    setting_startyear.text = str(x1)
    setting_endyear = ET.SubElement(setting, "endyear")        
    setting_endyear.text = str(x1)
    setting_sysitem = ET.SubElement(setting, "sysitem")
    setting_sysitem.text = str(x2)
    #custdata
    custdata_custname = ET.SubElement(custdata, "custname")
    custdata_custname.text = str(x3)
    custdata_appellation = ET.SubElement(custdata, "appellation")
    custdata_appellation.text = str(x4)
    custdata_sex = ET.SubElement(custdata, "sex")
    custdata_sex.text = str(x5)
    custdata_sex = ET.SubElement(custdata, "birth")
    custdata_sex.text = str(x6)
    custdata_pa = ET.SubElement(custdata, "pa")
    custdata_pa.text = str(x7)
    
    #subroot
    polmdata = ET.SubElement(custdata, "polmdata")
    #polmdata
    polmdata_polno = ET.SubElement(polmdata, "polno")
    polmdata_polno.text = str(x8)
    polmdata_company = ET.SubElement(polmdata, "company")
    polmdata_company.text = str(x9)
    polmdata_accdate = ET.SubElement(polmdata, "accdate")
    polmdata_accdate.text = str(x10)
    polmdata_ownername = ET.SubElement(polmdata, "ownername")
    polmdata_ownername.text = str(x11)        
    polmdata_mode = ET.SubElement(polmdata, "mode")
    polmdata_mode.text = str(x12)        
    
    #thirdroot
    pdtdata = ET.SubElement(polmdata, "pdtdata")
    #pdtdata
    pdtdata_kind = ET.SubElement(pdtdata, "kind")
    pdtdata_kind.text = str(x13)
    pdtdata_pdtcode = ET.SubElement(pdtdata, "pdtcode")
    pdtdata_pdtcode.text = str(x14)
    pdtdata_pdtyear = ET.SubElement(pdtdata, "pdtyear")
    pdtdata_pdtyear.text = str(x15)        
    pdtdata_relation = ET.SubElement(pdtdata, "relation")
    pdtdata_relation.text = str(x16)              
    pdtdata_relano = ET.SubElement(pdtdata, "relano")
    pdtdata_relano.text = str(x17)
    pdtdata_amtinput = ET.SubElement(pdtdata, "amtinput") 
    pdtdata_amtinput.text = str(x18)        
    pdtdata_modeprem = ET.SubElement(pdtdata, "modeprem")
    pdtdata_modeprem.text = str(x19)               
    pdtdata_plstatuscode = ET.SubElement(pdtdata, "plstatuscode")
    pdtdata_plstatuscode.text = str(x20)               
    pdtdata_plstatuscode = ET.SubElement(pdtdata, "currency")
    pdtdata_plstatuscode.text = str(x21)      
    pdtdata_plstatuscode = ET.SubElement(pdtdata, "rate")
    pdtdata_plstatuscode.text = str(x22)      
    
    
    tree = ET.ElementTree(root)
    xmlstr = minidom.parseString(ET.tostring(root)).toprettyxml(indent=" ").replace('\n','')
    
    return xmlstr
        
def GetCoverageResult():
    record_time = [datetime.now()]
    url = api_url+tokenkey
    payload={}
    headers = header_keys
    response = requests.request("GET", url, headers=headers, data=payload)
    res = response.text
    # For DreamFactory API header settings(05)
    sessionid = response.headers['Set-Cookie']
    values_new = ['',sessionid]
    header_new = dict(zip(keys,values_new))
    df_final = pd.DataFrame(columns = cvr_cols)
    
    for index,url in enumerate(policy_data['xml']):
        url = url
        payload={}
        headers = header_new
        response = requests.request("GET", url, headers=headers, data=payload)
        xml_data=response.text
        data = []
        cols = []
        try:
            root = ET.XML(xml_data)
            if xml_data!="<?xml version='1.0' encoding='utf-8'?><ensurelist></ensurelist>":

                    if re.search('Error|錯誤|value',xml_data)==None:
                      # Parse XML
                        for i, child in enumerate(root):
                            if child.tag=='Year':
                                for subchild in child:
                                    data.append(subchild.text)
                                    cols.append(subchild.tag)
                            else:
                                data.append(np.nan)
                                cols.append('null')  
                    else:
                            data.append(np.nan)
                            cols.append('null')                 
            else :
                    data.append(np.nan)
                    cols.append('null')
        except:
                data.append(np.nan)
                cols.append('null') 
        df_pre = pd.DataFrame(data).T  # Write in DF and transpose it
        df_pre.columns = cols  # Update column names
        df_final = pd.concat([df_final,df_pre])
        df_final.reset_index(drop = True,inplace=True)
    result = pd.merge(policy_data[pol_cols], df_final[cvr_cols], left_index=True, right_index=True)
    result.reset_index(drop = True,inplace=True)
    record_time.append(datetime.now())
    msg = 'Getting response data spend {0} minutes,Total request {1} records. Get {2} records'.format(diff_time(record_time),index+1,result.shape[0])
    print(msg)
    return result

def transfer_out():
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()
    with sftp_client.open(output_path+output_file, "w") as f:
        f.write(result.to_csv(index=False, encoding='utf-8', date_format='%Y%m%d',sep='|'))
    print(f'{output_path+output_file} is sent!')
    
if __name__ == '__main__':

    record_time = [datetime.now()]
    #Load data
    policy_data = transfer_in()

    #Data preprocessing
    policy_data[pre_cols] = policy_data[pre_cols].replace('',' ')
    policy_data['combined'] = policy_data[combine_cols].apply(lambda row:processString(','.join(row.values.astype(str))), axis=1 )
    policy_data['combined'] = policy_data['combined'].replace('',' ')
    
    #Build API input(XML)
    policy_data['xml'] = np.vectorize(buildNewXml)(
    policy_data['current_year']
    ,policy_data['combined']
    ,policy_data['client_i_name']
    ,policy_data['rela_seq']
    ,policy_data['client_i_sex']
    ,policy_data['client_i_birth']
    ,policy_data['occupation_class']
    ,policy_data['policy_no']
    ,policy_data['company']
    ,policy_data['co_issue_date']
    ,policy_data['client_i_name']
    ,policy_data['modx']
    ,policy_data['kind']
    ,policy_data['pdtcode']  
    ,policy_data['pdtyear'] 
    ,policy_data['client_i_name']
    ,policy_data['rela_seq']
    ,policy_data['face_amt_unit']
    ,policy_data['mode_prem']
    ,policy_data['plstatuscode']
    ,policy_data['currency_desc']
    ,policy_data['exg_rate']
    )
    policy_data['xml'] = str(request_url)+policy_data['xml']
    
    #Get Coverage result responses
    result = GetCoverageResult()
    
    #Transfer data to GP JCS
    transfer_out()