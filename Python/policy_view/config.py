import pandas as pd
import time
from datetime import datetime
from argparse import ArgumentParser


##############################
#       JCS sftp settings    #
##############################

HOST='' #test
PORT=22
USERNAME=''
PS='' #test

##############################
#        JCS File Path       #
##############################

#argument parser settings
parser = ArgumentParser()
parser.add_argument("--poyear", help="policy view policy issue year",required=True)
args = parser.parse_args()
poyear = args.poyear   


input_file='plcy_val_{}.csv'.format(args.poyear)
output_path=''

output_file='policy_result_{}.csv'.format(args.poyear)

##############################
#        API Settings        #
##############################

api_url = "" #DreamFactory URL
tokenkey = ""
request_url = "" #DreamFactory URL

# For DreamFactory API header settings(01)
keys = ['X-DreamFactory-API-Key','Cookie']
values = ['','']
header_keys = dict(zip(keys,values))



############################################
#       Data Types and columns settings    #
############################################


policy_data_dtypes = {
'row_number':'int64',
'policy_no':'object',
'coverage_no':'int64',
'collect_po_type':'object',
'po_issue_date':'object',
'co_issue_date':'object',
'matured_date':'object',
'expired_date':'object',
'po_year':'int64',
'co_status_code':'object',
'rate_sex':'object',
'rate_age':'int64',
'plan_code':'object',
'version':'object',
'face_amt':'object',
'amtunit':'object',
'modx':'object',
'mode_prem':'int64',
'occupation_class':'object',
'client_id_o':'object',
'client_o_name':'object',
'client_o_birth':'object',
'client_o_sex':'object',
'client_id_i':'object',
'client_i_name':'object',
'client_i_birth':'object',
'client_i_sex':'object',
'rela_seq':'object',
'pdtcode':'object',
'kind':'object',
'currency_1':'object',
'currency_desc':'object',
'exg_rate':'int64',
'plstatuscode':'object',
'company':'object',
'a1':'object',
'a2':'object',
'a3':'object',
'a4':'object',
'a5':'object',
'a6':'object',
'a7':'object',
'a8':'object',
'a10':'object',
'a11':'object',
'a12':'object',
'a13':'object',
'a14':'object',
'a15':'object',
'a17':'object',
'a18':'object',
'a19':'object',
'a20':'object',
'a21':'object',
'a22':'object',
'a23':'object',
'a24':'object',
'a25':'object',
'a26':'object',
'a27':'object',
'a28':'object',
'a29':'object',
'a30':'object',
'a31':'object',
'b1':'object',
'b2':'object',
'b3':'object',
'b4':'object',
'b5':'object',
'b6':'object',
'b7':'object',
'b8':'object',
'b9':'object',
'b10':'object',
'b11':'object',
'b12':'object',
'b13':'object',
'b14':'object',
'c1':'object',
'c2':'object',
'c3':'object',
'c4':'object',
'c5':'object',
'c6':'object',
'c7':'object',
'c8':'object',
'c10':'object',
'c11':'object',
'c12':'object',
'c13':'object',
'c14':'object',
'c15':'object',
'd1':'object',
'd2':'object',
'd3':'object',
'd4':'object',
'd5':'object',
'e1':'object',
'e2':'object',
'e3':'object',
'e4':'object',
'e5':'object',
'e6':'object',
'e7':'object',
'e8':'object',
'e9':'object',
'e10':'object',
'e11':'object',
'e12':'object',
'e13':'object',
'e14':'object',
'e15':'object',
'e16':'object',
'e17':'object',
'e18':'object',
'e19':'object',
'e20':'object',
'current_year':'int64'
}


combine_cols  = ['a1'
,'a2'
,'a3'
,'a4'
,'a5'
,'a6'
,'a7'
,'a8'
,'a10'
,'a11'
,'a12'
,'a13'
,'a14'
,'a15'
,'a17'
,'a18'
,'a19'
,'a20'
,'a21'
,'a22'
,'a23'
,'a24'
,'a25'
,'a26'
,'a27'
,'a28'
,'a29'
,'a30'
,'a31'
,'b1'
,'b2'
,'b3'
,'b4'
,'b5'
,'b6'
,'b7'
,'b8'
,'b9'
,'b10'
,'b11'
,'b12'
,'b13'
,'b14'
,'c1'
,'c2'
,'c3'
,'c4'
,'c5'
,'c6'
,'c7'
,'c8'
,'c10'
,'c11'
,'c12'
,'c13'
,'c14'
,'c15'
,'d1'
,'d2'
,'d3'
,'d4'
,'d5'
,'e1'
,'e2'
,'e3'
,'e4'
,'e5'
,'e6'
,'e7'
,'e8'
,'e9'
,'e10'
,'e11'
,'e12'
,'e13'
,'e14'
,'e15'
,'e16'
,'e17'
,'e18'
,'e19'
,'e20'
]

pre_cols = [
'row_number'
,'policy_no'
,'coverage_no'
,'po_issue_date'
,'co_issue_date'
, 'matured_date'
, 'expired_date'
, 'po_year'
,'co_status_code'
, 'rate_sex'
, 'rate_age'
, 'plan_code'
, 'version'
, 'face_amt'
, 'amtunit'
, 'face_amt_unit'
, 'modx'
, 'mode_prem'
, 'occupation_class'
, 'client_id_o'
, 'client_o_name'
, 'client_o_birth'
, 'client_o_sex'
, 'client_id_i'
, 'client_i_name'
, 'client_i_birth'
, 'client_i_sex'
, 'rela_seq'
, 'pdtcode'
, 'kind'
, 'currency_1'
, 'exg_rate'
, 'plstatuscode'
, 'company']
       
       
pol_cols =[
'row_number'
,'policy_no'
,'coverage_no'
,'collect_po_type'
,'po_issue_date'
,'co_issue_date'
,'matured_date'
,'expired_date'
,'po_year'
,'co_status_code'
,'rate_sex'
,'rate_age'
,'plan_code'
,'version'
, 'face_amt'
,'amtunit'
,'face_amt_unit'
,'modx'
,'mode_prem'
,'occupation_class'
,'client_id_o'
,'client_o_name'
,'client_o_birth'
,'client_o_sex'
,'client_id_i'
,'client_i_name'
,'client_i_birth'
,'client_i_sex'
,'rela_seq'
,'pdtcode'
,'kind'
,'currency_1'
,'currency_desc'
,'exg_rate'
,'plstatuscode'
,'company'
,'data_dt'
,'combined'
,'xml' ]


cvr_cols = [
'pdtcode'
,'A1'
,'A2'
,'A3'
,'A4'
,'A5'
,'A6'
,'A7'
,'A8'
,'A10'
,'A11'
,'A12'
,'A13'
,'A14'
,'A15'
,'A17'
,'A18'
,'A19'
,'A20'
,'A21'
,'A22'
,'A23'
,'A24'
,'A25'
,'A26'
,'A27'
,'A28'
,'A29'
,'A30'
,'A31'
,'B1'
,'B2'
,'B3'
,'B4'
,'B5'
,'B6'
,'B7'
,'B8'
,'B9'
,'B10'
,'B11'
,'B12'
,'B13'
,'B14'
,'C1'
,'C2'
,'C3'
,'C4'
,'C5'
,'C6'
,'C7'
,'C8'
,'C10'
,'C11'
,'C12'
,'C13'
,'C14'
,'C15'
,'D1'
,'D2'
,'D3'
,'D4'
,'D5'
,'E1'
,'E2'
,'E3'
,'E4'
,'E5'
,'E6'
,'E7'
,'E8'
,'E9'
,'E10'
,'E11'
,'E12'
,'E13'
,'E14'
,'E15'
,'E16'
,'E17'
,'E18'
,'E19'
,'E20'
]