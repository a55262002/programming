#!bin/bash


################################################################################
# we need to assign the container name with our predict model's container name #
# container name = model's container name                                      #
################################################################################
containerName="repur_hdp"

##############
# print logs #
##############
echo "container name is $containerName "

################################################
# check the docker container is running or not #
################################################
if docker stats --no-stream | grep $containerName
then 
    ##################################################
    # if model's container is running print the logs #
    ##################################################
    echo " model's container is still running "

    #######################################################
    # first of all, we need to stop the model's container # 
    #######################################################
    echo "model's container is stopping"
    docker stop $containerName
    echo " model's container is stopped"

#########################################################
# container is already stopped, so we don't do anything #
#########################################################
else 
    echo "model's container is not running"
fi

