from config import *
from System.data_utils import *
from System.utils import *
from System.metrics import *
from System.model import *
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, LabelEncoder
import tensorflow as tf
import pandas as pd
import numpy as np
import os
import shutil
import psutil
import time
import random
import pickle
import json
import gc
from collections import OrderedDict
from datetime import date, datetime
from pprint import pprint
import platform
from sqlalchemy import create_engine
from sqlalchemy.sql import select, text
import math
import glob
import warnings
pd.set_option('display.max_columns', 9999)
pd.set_option('display.max_rows', 9999)
warnings.filterwarnings("ignore")


def diff_time(record):
    return round(((record[-1] - record[-2]).seconds) / 60, 2)


def loading_data(table_dict, conn, update=True, Procedure_Test=False):
    record_time = [datetime.now()]
    if update:
        if Procedure_Test:
              file_train = pd.read_csv('data/Input/df_data.csv',sep = '|', usecols=[x.lower() for x in remain_co_stda])
              df_data = pd.DataFrame(file_train)
              df_data.columns = map(str.upper, df_data.columns)
        else:
            sql_train = load_sql(table_dict['co_stda'][0])
        data_content = conn.execute(text(sql_train))
        data_columns = [i.upper() for i in data_content.keys()]
        df_data = pd.DataFrame(data_content.fetchall(), columns=data_columns)
        df_data = df_data[table_dict['co_stda'][1]]
        df_data.to_csv(os.path.join(cache_path, 'df_data.csv'), index=False)

        record_time.append(datetime.now())
        msg = 'download co_stda data spend {} minutes, co_stda shape {}'.format(
            diff_time(record_time), df_data.shape)
        write_log('trainging_history.csv', msg)

        plan_content = conn.execute(text(load_sql(table_dict['plandefi'][0])))
        plan_columns = [i.upper() for i in plan_content.keys()]
        df_plan = pd.DataFrame(plan_content.fetchall(), columns=plan_columns)
        df_plan = df_plan[table_dict['plandefi'][1]]
        df_plan.to_csv(os.path.join(cache_path, 'df_plan.csv'), index=False)

        record_time.append(datetime.now())
        msg = 'download plandefi data spend {} minutes, plandefi shape {}'.format(
            diff_time(record_time), df_plan.shape)
        write_log('trainging_history.csv', msg)

        # test data
        if Procedure_Test:
            sql_test = load_sql(
                table_dict['recommend'][0]) + ' limit {}'.format(Test_row)
        else:
            sql_test = load_sql(table_dict['recommend'][0])
        test_content = conn.execute(text(sql_test))
        test_columns = [i.upper() for i in test_content.keys()]
        df_test = pd.DataFrame(test_content.fetchall(), columns=test_columns)
        df_test.to_csv(os.path.join(cache_path, 'df_test.csv'), index=False)

        record_time.append(datetime.now())
        msg = 'download test co_stda data spend {} minutes, test co_stda shape {}'.format(
            diff_time(record_time), df_test.shape)
        write_log('trainging_history.csv', msg)

    df_data = pd.read_csv(os.path.join(cache_path, 'df_data.csv'))
    df_plan = pd.read_csv(os.path.join(cache_path, 'df_plan.csv'))
    df_test = pd.read_csv(os.path.join(cache_path, 'df_test.csv'))

    record_time.append(datetime.now())
    msg = 'loading data spend {} minutes'.format(diff_time(record_time))
    write_log('trainging_history.csv', msg)
    return df_data, df_plan, df_test

def loading_data_v2(table_dict,update=True, Procedure_Test=False):
    record_time = [datetime.now()]
    if update:
        if Procedure_Test:
              file_train = pd.read_csv('data/Input/df_data.csv',sep = '|', usecols=[x.lower() for x in remain_co_stda])
              df_data = pd.DataFrame(file_train)
              df_data.columns = map(str.upper, df_data.columns)
        else:
              file_train = pd.read_csv('data/Input/df_data.csv',sep = '|', usecols=[x.lower() for x in remain_co_stda])
              df_data = pd.DataFrame(file_train)
              df_data.columns = map(str.upper, df_data.columns)

        record_time.append(datetime.now())
        msg = 'download co_stda data spend {} minutes, co_stda shape {}'.format(
            diff_time(record_time), df_data.shape)
        write_log('trainging_history.csv', msg)

        file_plan = pd.read_csv('data/Input/df_plan.csv',sep = '|', usecols=[x.lower() for x in remain_plandefi])

        df_plan = pd.DataFrame(file_plan)
        for i in [1.0,2.0,3.0,4.0,5.0,8.0]:
            df_plan.loc[df_plan['plan_relation']==i] = str(int(i))

        # df_plan = df_plan[table_dict['plandefi'][1]]

        df_plan.columns = map(str.upper, df_plan.columns)
        df_plan.fillna({'PLAN_RELATION': '1', 'INSURANCE_TYPE':'D'}, inplace=True) #add
        
        df_plan['PRIMARY_RIDER_IND']=df_plan['PRIMARY_RIDER_IND'].astype(str)

        record_time.append(datetime.now())
        msg = 'download plandefi data spend {} minutes, plandefi shape {}'.format(
            diff_time(record_time), df_plan.shape)
        write_log('trainging_history.csv', msg)

        # test data
        if Procedure_Test:
            file_test = pd.read_csv('data/Input/df_test.csv',sep = '|')
            df_test = pd.DataFrame(file_test)
        else:
            file_test = pd.read_csv('data/Input/df_test.csv',sep = '|')
            df_test = pd.DataFrame(file_test)

        df_test.columns = map(str.upper, df_test.columns)


        df_data = df_data.astype(df_data_dtypes)
        df_plan = df_plan.astype(df_plan_dtypes)
        df_test = df_test.astype(df_test_dtypes)

        record_time.append(datetime.now())
        msg = 'download test co_stda data spend {} minutes, test co_stda shape {}'.format(
            diff_time(record_time), df_test.shape)
        write_log('trainging_history.csv', msg)


    record_time.append(datetime.now())
    msg = 'loading data spend {} minutes'.format(diff_time(record_time))
    write_log('trainging_history.csv', msg)
    return df_data, df_plan, df_test

def preprocess_data(df_data, df_plan, df_test, cache_path, update=True):
    record_time = [datetime.now()]

    if update:
    # %% Data Pre
        df_plan = df_plan.astype(df_plan_dtypes)
        df_plandefi = pre_plandefi(df_plan)
        df_plandefi, item_enc, plan_cols_enc_dict, plan_cols_minmax_dict = preprocess_plandefi(
            df_plandefi, cache_path)

        record_time.append(datetime.now())
        msg = 'preprocess plandefi data spend {} minutes, preprocess plandefi shape {}'.format(
            diff_time(record_time), df_plandefi.shape)
        write_log('trainging_history.csv', msg)

        df_data = fill_value_dtype(df_data, df_data_dtypes)
        df_co_stda = pre_co_stda(df_data)
        df_co_stda_all = read_co_stda_df(df_co_stda)
        df_co_stda_all, stda_cols_enc_dict = preprocess_co_stda_all(
            df_co_stda_all, df_plandefi, item_enc, cache_path)

        record_time.append(datetime.now())
        msg = 'preprocess co stda data but not contain also buy dict spend {} minutes'.format(
            diff_time(record_time))
        write_log('trainging_history.csv', msg)

        also_buy_dict, past_buy_dict = gen_also_buy_past_buy_dict(
            df_co_stda_all, cache_path)

        record_time.append(datetime.now())
        msg = 'preprocess co_stda_all data spend {} minutes, preprocess co_stda_all shape {}'.format(
            diff_time(record_time), df_co_stda_all.shape)
        write_log('trainging_history.csv', msg)

        df_co_train = read_co_stda_train_test(df_co_stda, cache_path)

        record_time.append(datetime.now())
        msg = 'read_co_stda_train_test spend {} minutes'.format(
            diff_time(record_time))
        write_log('trainging_history.csv', msg)

        # %% preprocess
        df_co_train, INSURED_AGE_minmax_scale = preprocess_co_stda(
            df_co_stda,
            df_plandefi,
            stda_cols_enc_dict,
            item_enc,
            cache_path,
        )  # 裡面會有一點點負樣本

        record_time.append(datetime.now())
        msg = 'preprocess co_train spend {} minutes, preprocess co_stda shape {}'.format(
            diff_time(record_time), df_co_train.shape)
        write_log('trainging_history.csv', msg)
        # %% generate negative data
        df_co_train = lookup_also_buy_past_buy_dict(
            df_co_train, also_buy_dict, past_buy_dict, cache_path)

        record_time.append(datetime.now())
        msg = 'lookup_also_buy_past_buy_dict spend {} minutes'.format(
            diff_time(record_time))
        write_log('trainging_history.csv', msg)

        num = math.floor(df_co_train.shape[0] * sample_ratio)
        ##################################test##################################
        # df_co_train.to_csv('gen_1_to_k_df_co_train.csv', index=False)
        # df_plandefi.to_csv('gen_1_to_k_df_plandefi.csv', index=False)
        # with open('gen_1_to_k_num.csv', 'w', encoding='utf8') as f:
        #     f.write(str(num))
        # f.close()
        ##################################test##################################

        df_train, df_valid = gen_negative_data_1_to_k(df_co_train, df_plandefi, cache_path, subpath=sub_path,
                                                      prefix=prefix, chunk_size=chunk_size, num_sample=num, num_gen=num_expend, split_ratio=0.33)

        record_time.append(datetime.now())
        msg = 'gen_negative_data_1_to_k spend {} minutes'.format(
            diff_time(record_time))
        write_log('trainging_history.csv', msg)

        df_train, df_valid = merge_plan(df_train, df_valid, df_plandefi)

        record_time.append(datetime.now())
        msg = 'split train valid data spend {} minutes, train data shape {}, valid data shape {}'.format(
            diff_time(record_time), df_train.shape, df_valid.shape)
        write_log('trainging_history.csv', msg)
        # %% Data Pre

        df_test = fill_value_dtype(df_test, {'INSURED_SEX': 'int64', 'PO_ISSUE_DATE': 'object'})
        df_test = preprocess_co_stda_test(df_test, df_plandefi,
                                          item_enc, stda_cols_enc_dict, INSURED_AGE_minmax_scale,
                                          also_buy_dict, past_buy_dict, has_label=True)
        save_list = [df_train, df_valid, df_test, df_plandefi,
                     item_enc, also_buy_dict, past_buy_dict]
        save_data(save_list, cache_path)

        record_time.append(datetime.now())
        msg = 'preprocess test data spend {} minutes, test data shape {}'.format(
            diff_time(record_time), df_test.shape)
        write_log('trainging_history.csv', msg)

    if not update:
        df_train, df_valid, df_test, df_plandefi, item_enc, also_buy_dict, past_buy_dict = load_data(cache_path)
        record_time.append(datetime.now())
        msg = 'loading data spend {} minutes'.format(diff_time(record_time))
        write_log('trainging_history.csv', msg)

    return df_train, df_valid, df_test, df_plandefi, item_enc, also_buy_dict, past_buy_dict


def concat_output(tmp_output):
    concat_list = glob.glob(os.path.join(tmp_output, '*.csv'))

    predict_df = pd.DataFrame()

    for file in concat_list:
        tmp_df = pd.read_csv(file)
        predict_df = pd.concat([predict_df, tmp_df], axis=0)

    predict_df.reset_index(drop=True, inplace=True)
    predict_path = os.path.join(tmp_output, predict_result + '_ori.csv')
    predict_df.to_csv(predict_path, index=False)

    return predict_df


def re_arrange(df, k, result_folder, predict_result):
    counter = 0
    columns_plan_list = []
    columns_pred_list = []
    for i in range(k):
        columns_plan_list.append('recommend_plan_' + str(i + 1))
        columns_pred_list.append('recommend_pred_' + str(i + 1))
    df_pred['highest_k'] = df_pred['highest_k'].apply(lambda x: x.replace(
        '[', '').replace(']', '').replace('\"', '').replace('\'', '').split(' '))
    df_pred['highest_k_pred'] = df_pred['highest_k_pred'].apply(lambda x: x.replace(
        '[', '').replace(']', '').replace('\"', '').replace('\'', '').replace(',', '').split(' '))

    df[columns_plan_list] = pd.DataFrame(
        df['highest_k'].tolist(), index=df.index)
    df[columns_pred_list] = pd.DataFrame(
        df['highest_k_pred'].tolist(), index=df.index)
    df.drop(columns=['also_insura_ids', 'highest_k',
                     'highest_k_pred', 'hit', 'num_also_buy'], inplace=True)
    predict_path = os.path.join(result_folder, predict_result + '.csv')
    df.to_csv(predict_path, index=False)

def re_arrange_v2(df, k, result_folder, predict_result):
    counter = 0
    columns_plan_list = []
    columns_pred_list = []
    for i in range(k):
        columns_plan_list.append('recommend_plan_' + str(i + 1))
        columns_pred_list.append('recommend_pred_' + str(i + 1))
    df_pred['highest_k'] = df_pred['highest_k'].apply(lambda x: x.replace(
        '[', '').replace(']', '').replace('\"', '').replace('\'', '').split(' '))
    df_pred['highest_k_pred'] = df_pred['highest_k_pred'].apply(lambda x: x.replace(
        '[', '').replace(']', '').replace('\"', '').replace('\'', '').replace(',', '').split(' '))

    df[columns_plan_list] = pd.DataFrame(
        df['highest_k'].tolist(), index=df.index)
    df[columns_pred_list] = pd.DataFrame(
        df['highest_k_pred'].tolist(), index=df.index)
    df.drop(columns=['also_insura_ids', 'highest_k',
                     'highest_k_pred', 'hit', 'num_also_buy'], inplace=True)
    if not os.path.exists(result_folder):
         os.makedirs(result_folder)
         predict_path = os.path.join(result_folder, predict_result + '.csv')
    else:
         predict_path = os.path.join(result_folder, predict_result + '.csv')
         
    return  df.to_csv(predict_path, index=False)

def upload_dw(file, upload_folder, curs, conn, table):
    upload_path = os.path.join(upload_folder, file)
    update_sql = open('upload.sql', 'r', encoding='utf-8')
    sql_rd = update_sql.read()
    update_sql.close()
    sql_rd = sql_rd.replace('target_table', table)
    sql_rd = sql_rd.replace('path', upload_path)
    curs.execute(sql_rd)
    conn.commit()


def batch_predict(model, k, chunk, batch_size, df_plandefi):
    # df_plandefi.to_csv('df_plandefi.csv', index=False)
    counter = 0
    track = ['TestGen', 'pred', 'load decode', 'mask', 'pred*mask', 'df_te', 'recall']
    df_recom = pd.DataFrame()
    total_rows = 0
    for chunk in range(len(test_chunk)):
        total_rows += test_chunk[chunk].shape[0]

    for chunk in range(len(test_chunk)):
        # start
        track_time = [datetime.now()]
        test_data = test_chunk[chunk]
        file_name = os.path.join(
            tmp_output, 'recomm_{}_{}_{}'.format(k, suffix_date, counter))
        TestGen = DataGenerator(data=test_data,  # batch by slice chunk list
                                also_buy_dict=also_buy_dict,
                                past_buy_dict=past_buy_dict,
                                multi_cols=['past_insura_ids'],
                                df_plandefi=df_plandefi,
                                n_batch=batch_size,
                                shuffle=False)  # 測試時不可亂數!!!

        # TestGen
        track_time.append(datetime.now())

        with tf.Session(graph=model.graph) as sess:
            pred = model.predict(sess, TestGen.gen_test(), df_plandefi)

        # pred
        track_time.append(datetime.now())

        dict_encoding_name, dict_code_name = gen_dict_encoding_code_name(df_plandefi, item_enc)
        ############################################
        # with open('dict_code_name.pkl', 'wb') as handle:
        #     pickle.dump(dict_code_name, handle, protocol=pickle.HIGHEST_PROTOCOL)
        ############################################
        plan_to_idx_dict = gen_plan_to_idx_dict(df_plandefi)

        track_time.append(datetime.now())
        # load decode


        # pred_avail_mask = gen_pred_avail_mask(TestGen, pred, df_plandefi) # tune
        pred_can_buy = gen_pred_avail_mask(TestGen, pred, df_plandefi)

        # mask
        track_time.append(datetime.now())

        # 將被保人在投保當下無法購買的商品，其預測購買機率皆以mask遮罩為0
        # PO_ISSUE_DATE 若改成目前的日期，就會產生目前可購買的推薦結果

        # pred_can_buy = pred * pred_avail_mask # tune
        pred_can_buy *= pred # reduce memory effort
        # pd.DataFrame(pred_can_buy).to_csv('df_pred_can_buy.csv', index=False)

        ## pred * mask
        track_time.append(datetime.now())

        df_te = pd.DataFrame(TestGen.gen_test())
        # df_te.to_csv('df_te.csv', index=False)

        # df_te
        track_time.append(datetime.now())
        df_recomm_10 = recall_at_k(list(df_te['USER']), list(
            df_te['also_insura_ids']), pred_can_buy, df_plandefi['PLAN_CODE_1'], item_enc, dict_code_name, suffix_date, file_name, k=k)

        ## output & recall
        track_time.append(datetime.now())
        counter += 1
        execute_time = [(x - y).seconds for x,
                        y in zip(track_time[1:], track_time[:-1])]
        cc = 0
        msg = ''
        for i in track:
            msg += (i + ' ' + str(round(execute_time[cc] / 60, 4)) + ' mins ; ')
            cc += 1
        total_rows -= test_data.shape[0]
        write_log('trainging_history.csv', msg)
        msg = 'Chunk {} batch predict {} rows, left {} rows'.format(counter, test_data.shape[0], total_rows)
        write_log('trainging_history.csv', msg)
    return


if __name__ == '__main__':

    record_time = [datetime.now()]
    msg = ('Main Start Procudure in {}'.format(
        platform.system() + ' ' + platform.release()))
    if Procedure_Test:
        msg = 'Procedure Test \n' + msg
    write_log('trainging_history.csv', msg)


    resetFolder(tmp_output)

    table_dict = {
        'co_stda': [table_co_stda, remain_co_stda],
        'plandefi': [table_plan, remain_plandefi],
        'recommend': [test_co_stda]
    }

    df_data, df_plan, df_test = loading_data_v2(
        table_dict,  update=Update, Procedure_Test=Procedure_Test)

    df_plan = df_plan.astype(df_plan_dtypes)
    df_data = df_data.astype(df_data_dtypes)
    df_test = df_test.astype(df_test_dtypes)

    record_time.append(datetime.now())
    msg = 'Main Loading all data spend {} minutes'.format(
        diff_time(record_time))
    write_log('trainging_history.csv', msg)

    df_train, df_valid, df_test, df_plandefi, item_enc, also_buy_dict, past_buy_dict = preprocess_data(
        df_data, df_plan, df_test, cache_path)

    df_train = df_train.dropna()
    df_valid = df_valid.dropna()
    df_test = df_test.dropna()

    record_time.append(datetime.now())
    msg = 'Main Preprocess all data spend {} minutes'.format(diff_time(record_time))
    write_log('trainging_history.csv', msg)
    # %% data generatot
    TrainGen = DataGenerator(data=df_train,
                             also_buy_dict=also_buy_dict,
                             past_buy_dict=past_buy_dict,
                             multi_cols=['past_insura_ids'],
                             df_plandefi=df_plandefi,
                             n_batch=batch_size,
                             shuffle=True)

    ValidGen = DataGenerator(data=df_valid,
                             also_buy_dict=also_buy_dict,
                             past_buy_dict=past_buy_dict,
                             multi_cols=['past_insura_ids'],
                             df_plandefi=df_plandefi,
                             n_batch=batch_size,
                             shuffle=True)

    # hyper parameters
    n_items = len(item_enc.classes_)
    # len_train = df_train.shape[0]
    len_train = df_train.shape[0]+1
    print(n_items, len_train)

    tf.reset_default_graph()
    model = ModelMfDNNCrossEntropy(n_items=n_items, len_train=len_train, dim=dim, batch_size=batch_size, pos_weight=pos_weight, learning_rate=learning_rate, modelDir=model_path )

    if Train:
        resetFolder(model_path)
        with tf.Session(graph=model.graph) as sess:
            model.fit(sess, TrainGen, ValidGen, nEpoch=epochs, reset=True)

        record_time.append(datetime.now())
        msg = 'Main Model training {} epochs spend {} minutes'.format(
            epochs, diff_time(record_time))
        write_log('trainging_history.csv', msg)

    if Predict:
        # df_test.to_csv('df_test.csv',index=False)
        test_chunk = split_dataframe(df_test, chunk_size=chunk_size)
        batch_predict(model, k, chunk=test_chunk, batch_size=batch_size, df_plandefi = df_plandefi)

        record_time.append(datetime.now())
        msg = 'Main Model Predict {} batchs spend {} minutes'.format(
            len(test_chunk), diff_time(record_time))
        write_log('trainging_history.csv', msg)

        df_pred = concat_output(tmp_output)
        re_arrange_v2(df_pred, k, result_folder, predict_result)
        resetFolder(tmp_output)
        record_time.append(datetime.now())
        msg = 'Main Concat Output File spend {} minutes'.format(
            diff_time(record_time))
        write_log('trainging_history.csv', msg)

    if Upload:
        try:
            df_pred.to_sql(upload_table, con=engine, index=False, if_exists='replace')

            record_time.append(datetime.now())
            msg = 'Main Upload DW spend {} minutes'.format(diff_time(record_time))
            write_log('trainging_history.csv', msg)
        except:
            record_time.append(datetime.now())
            msg = 'Main Upload DW spend {} minutes but Upload fail Q_Q'.format(diff_time(record_time))
            write_log('trainging_history.csv', msg)

    record_time.append(datetime.now())
    msg = 'Main Finish This Predict Procedure Total Spend {} Minutes'.format(
        (record_time[-1] - record_time[0]).seconds / 60)
    write_log('trainging_history.csv', msg)
