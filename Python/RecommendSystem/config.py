# main procedure
# Procedure Test
Procedure_Test = False
Test_row = 40000

# Data Download if Update is true then update test data
Update = True

# model procedure
Train = True
Predict = True

# upload to DW
Upload = False

# path setting
cache_path = 'data'
model_path = 'model'
suffix_date = 'output'
tmp_output = 'tmp_output'
result_folder = 'data/Output'
sub_path = 'data_1-k'
prefix = 'data_neg'
predict_result = 'predict_result'


table_co_stda = 'train_co_stda.sql'
table_plan = 'plandefi.sql'
test_co_stda = 'test_co_stda.sql'
# select = 'select * from {}'
upload_table = 'recommend_result'
# upload_table = 'recommend_result_ori'

# sample setting
sample_ratio = 0.1  # 抽樣比率
num_expend = 2  # 包含負樣本生成倍數，以抽樣資料進行負樣本生成

# model setting
dim = 16
batch_size = 128
pos_weight = 10
learning_rate = 1e-3
epochs = 50


# predict setting
k = 10
chunk_size = 20000  # batch predict chunk size

# setting columns dtypes
df_data_dtypes = {
    'POLICY_NO':'object',
    'COVERAGE_NO':'object',
    'PO_ISSUE_DATE':'object',
    'PLAN_CODE':'object',
    'VERSION':'object',
    'INSURED_ID':'object',
    'INSURED_SEX':'object',
    'INSURED_AGE':'object',
    'PO_STATUS_CODE':'object',
    'COMM_LINE_TYPE':'object',
    'OCCUPATION_CLASS':'object',
    'IS_RIDER':'int64',
    'UW_DECISION':'object'
}

df_plan_dtypes = {
    'PLAN_CODE':'object',
    'VERSION':'object',
    'PLAN_TITLE':'object',
    'PLAN_START_DATE':'object',
    'PLAN_END_DATE':'object',
    'PRIMARY_RIDER_IND':'object',
    'CL_PLAN_TYPE':'object',
    'PLAN_TYPE':'object',
    'LOAN_AVAL_IND':'object',
    'COLLECT_YEAR_IND':'object',
    'COLLECT_YEAR_PRD_IND':'object',
    'EXP_YEAR_IND':'object',
    'EXP_YEAR_PRD_IND':'object',
    'INSURANCE_TYPE':'object',
    'INSURANCE_TYPE_3':'object',
    'INSURANCE_TYPE_4':'object',
    'PLAN_RELATION':'object',
    'PLAN_RELATION_SUB':'object',
    'LBENF':'object',
    'SAVE_BENEF_IND':'object',
    'CURRENCY_1':'object',
    'PLAN_ACCOUNT_IND':'object',
    'CASH_IND':'object',
    'RVNU_IND':'object',
    'INV_IND':'object',
    'IS_REAL_PAY':'int64',
    'IS_ACC':'object',
    'MED_TYPE':'int64',
    'IS_FIX':'object',
    'IS_CANCER':'object',
    'IS_BIG_SICK':'object',
    'IS_BIG_SICK_CLAIM':'object',
    'IS_BIG_DEFI':'object',
    'BIG_SICK_CLAIM_RATE':'object',
    'LIFE_SUPPORT_CLAIM_RATE':'object',
    'CLO_IND':'int64',
    'CL_O_IND':'int64',
    'CL_O_RT':'object',
    'HO_IND':'int64',
    'H_O_IND':'int64',
    'H_O_RT':'object',
    'HM_O_IND':'int64',
    'HM_O_RT':'object',
    'O_IND':'int64',
    'O_RT':'object',
    'OA_IND':'int64',
    'SICK_IND':'int64',
    'SICK_H_TYPE':'int64',
    'SICK_H_IND':'int64',
    'SICK_H_RT':'object',
    'SICK_RH_IND':'int64',
    'SICK_RH_RT':'object',
    'SICK_FH_IND':'int64',
    'SICK_FH_RT':'object',
    'RSRV_IND':'object',
    'TRDTN_IND':'int64',
    'PROD6_TYPE':'int64',
    'INV_TYPE':'object',
    'INV_LINK_TYPE':'object',
    'ANNU_TYPE':'int64',
    'CUS_CLASS':'object'
}

df_test_dtypes = {
    'PO_ISSUE_DATE':'object',
    'PLAN_CODE':'object',
    'VERSION':'object',
    'IS_RIDER':'int64',
    'INSURED_ID':'object',
    'UW_DECISION':'object',
    'INSURED_SEX':'object',
    'INSURED_AGE':'object',
    'COMM_LINE_TYPE':'object',
    'OCCUPATION_CLASS':'object'
}
