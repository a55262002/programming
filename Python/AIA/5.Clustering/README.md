# 分群流程
#### 未篩選變數分群
- 使用批踢踢原始資料進行分群
- 資料預處理
- 使用五種分群演算法(KMeans++、BayesGaussianMixture、Hierarchical、DBSCAN、Birch)
- 總樣本抽樣10%，使用Silhouette_score評估分群績效
- 使用KMeans進行分群  
#### 使用RandomForesrtClassifer篩選變數重要性
- 預處理後資料使用RandomForesrtClassifer篩選重要變數
- 使用五種分群演算法
- 總樣本抽樣10%，使用Silhouette_score評估分群績效
- 使用KMeans進行分群
- 產出分群資料

## 說明
#### 來源資料表說明
- PTT_Data_All_v2.csv：批踢踢原始資料第二版 (重整興趣分類)
- PTT_Data_All_v3.csv：批踢踢原始資料第三版 (重整興趣分類)

#### 產出資料表說明
- PTT_Data_All_v2_1.csv：批踢踢原始資料第二版+KMEANS分3群結果 
- PTT_Data_All_v3_1.csv：批踢踢原始資料第三版+KMEANS分3群結果 
- PTT_Data_All_v2_1_4.csv：批踢踢原始資料第二版+KMEANS分4群結果 
- PTT_Data_All_v3_1_4.csv：批踢踢原始資料第三版+KMEANS分4群結果
- score.csv：使用原始資料之五種分群結果評估
- score_para_impo.csv：使用篩選重要變數後資料之五種分群結果評估
#### 程式說明
- Clustering.ipynb：分群程式
- Describe_Cluster.ipynb：分群資料敘述性統計程式

#### 資料表欄位說明
| variable | 變數名稱 | example |
|---------------|---------|----------|
| method | 分群方法 | KMEANS |
| score | 分群績效 | 0.5 |
| time(sec) | 執行時間(秒) | 50 |
