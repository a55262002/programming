from config import *
from datetime import datetime
import paramiko
import pandas as pd
import re
import os

# input data from JCS FTP
def transfer_in():
    s=paramiko.SSHClient()
    s.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    s.connect(HOST,PORT,USERNAME,PS)
    sftp_client = s.open_sftp()
    try:
        keyword = pd.read_csv(sftp_client.open(REMOTE),header = None)[0].tolist()
    except:
        keyword = pd.read_csv(sftp_client.open(PRE_REMOTE),header = None)[0].tolist()
    return keyword    
    
# count banwords in web URL& web title
def forbidden_cnt(text):
    counter = 0
    for ban in banlist:
        counter += len(re.findall(ban,text))
    return counter
    
 # calculate chinese words frequency in web content
def foreign_cnt(text):
    reg      =  u"([\u4e00-\u9fff\u3000-\u303f\ufb00-\ufffd]+)"
    pattern =   re.compile(reg)
    results =   pattern.findall(text)
    return len(results)